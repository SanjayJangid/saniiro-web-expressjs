const mssql = require('mssql');

/**************************
 * mssql config options 
 **************************/
const dbConfig = {
  server: process.env.DB_SEREVR,
  port: parseInt(process.env.DB_PORT),
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
  },
  options: {
    encrypt: false, // true, for azure
    trustServerCertificate: false, // change to true for local dev / self-signed certs
    enableArithAbort:  true,
    instancename:  'SQLEXPRESS'  // SQL Server instance name
  }
}


// Middleware for MS SQL Server connection
const connectToDatabase = async (req, res, next) => {
  try {
    const pool = await new mssql.ConnectionPool(dbConfig).connect();
    req.db = pool;
    next();
  } catch (err) {
    console.error(`MS SQL Server connection error: ${err}`);
    next(err);
  }
};

module.exports = connectToDatabase;
