const express = require("express");
const fs =require('fs');
const path = require('path');
const router = express.Router();

const viewRoutes_path = path.resolve(__dirname, "viewRoutes.json");
const viewRoutes = fs.readFileSync(viewRoutes_path);
let routes = JSON.parse(viewRoutes.toString());


/* GET home page. */
router.get(routes.index.route, async (req, res, next) => {
  res.render(routes.index.view, { title: routes.index.title });
});

router.get(routes.about.route, function (req, res, next) {
  res.render(routes.about.view, { title: routes.about.title });
});

router.get(routes.contact.route, function (req, res, next) {
  res.render(routes.contact.view, { title: routes.contact.title });
});

router.get(routes.team.route, function (req, res, next) {
  res.render(routes.team.view, { title: routes.team.title });
});

router.get(routes.partner.route, function (req, res, next) {
  res.render(routes.partner.view, { title: routes.partner.title });
});

router.get(routes.error.route, function (req, res, next) {
  res.render(routes.error.view, { title: routes.error.title });
});

router.get(routes.whatsNew.route, function (req, res, next) {
  res.render(routes.whatsNew.view, { title: routes.whatsNew.title });
});

router.get(routes.requestDemo.route, function (req, res, next) {
  res.render(routes.requestDemo.view, { title: routes.requestDemo.title });
});

module.exports = router;
