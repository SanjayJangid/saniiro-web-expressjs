const HomeRestController = require("../../controllers/restController/HomeRestController");
const fs =require('fs');
const path = require('path');
const router = require("express").Router();

const apiRoutes_path = path.resolve(__dirname,"..", "apiRoutes.json");
const apiDataRoutes = fs.readFileSync(apiRoutes_path);
let apiRoutes = JSON.parse(apiDataRoutes.toString());

// Define user routes
router.get(apiRoutes.api.v1.getCountryCodes.route, HomeRestController.getAllCountryCodes);
router.post(apiRoutes.api.v1.postContactDetail.route, HomeRestController.postContactDetails);


module.exports = router;
