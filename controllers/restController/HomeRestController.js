const mssql = require('mssql');
module.exports = {
  getAllCountryCodes: async (req, res, next) => {
    try {
      const pool = req.db;
      const result = await pool
        .request()
        .query("select cc.Iso, cc.Phonecode, cc.Length from CountryWithCode as cc");
  
      // Process and send the database results to the client
      res.json(result.recordset);
    } catch (error) {
      console.log(error);
    }
  },

  postContactDetails: async (req, res, next) => {
    try {
      const pool = req.db;

      // Define the stored procedure name and parameters
      const procedureName = "Usp_Set_SaniiroWeb_Contact";
      // const parameters = [
      //   { name: "Name", type: mssql.NVarChar(50), value: req.body.name },
      //   { name: "Email", type: mssql.NVarChar(50), value: req.body.email },
      //   { name: "PhoneCode", type: mssql.NVarChar(50), value: req.body.phoneCode },
      //   { name: "MobileNo", type: mssql.NVarChar(50), value: req.body.mobile },
      //   { name: "usermessage", type: mssql.NVarChar(50), value: req.body.message },
      //   { name: "Type", type: mssql.NVarChar(50), value: req.body.type },
      //   { name: "RequestFrom", type: mssql.NVarChar(50), value: req.body.requetForm },
      //   // Add more parameters as needed
      // ];

      // Execute the stored procedure
      const result = await pool.request()
      .input('Name', mssql.NVarChar(100), req.body.name)
      .input('Email', mssql.NVarChar(500), req.body.email)
      .input('PhoneCode', mssql.NVarChar(10), req.body.phoneCode)
      .input('MobileNo', mssql.NVarChar(20), req.body.mobile)
      .input('usermessage', mssql.NVarChar(500), req.body.message)
      .input('Type', mssql.NVarChar(50), req.body.type)
      .input('RequestFrom', mssql.NVarChar(500), req.body.requetForm)
      .execute(procedureName);

      // Close the database connection
      await pool.close();

      console.log("SQL Response:", result);

      res.status(200).json({ message: "Data inserted successfully" });
    } catch (error) {
      // Handle database query errors
      console.error("Error inserting data:", error);
      res.status(500).json({ error: "Error inserting data" });
    }
  },
  // Other service functions
};