const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const expressLayouts = require('express-ejs-layouts');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const createError = require('http-errors');

/************************************
 * Set Environment variables first.
************************************/
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });
const connectToDatabase = require('./mssql.js');

const app = express();
// const mssql = require('mssql');
const host = process.env.HOST;
const port = process.env.PORT;




/******************************
 * Import Route handlers
 ******************************/

const viewRoutes = require('./routes/viewRoutes.js');
const apiRoutes = require('./routes/api');


/***********************
 * view engine setup 
 ***********************/ 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


/***********************
 * view layouts setup 
 ***********************/ 
app.use(expressLayouts);

// place all the metas, styles and scripts blocks
app.set("layout extractMetas", true)
app.set("layout extractScripts", true)
app.set("layout extractStyles", true)

app.use(compression());

const fs = require('fs');
const accessLogStream = fs.createWriteStream('access.log', { flags: 'a' });

// Log to a file using a writable stream
// app.use(logger('combined', { stream: accessLogStream }));
app.use(logger('dev', { stream: accessLogStream }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(connectToDatabase);


/*******************************************
 * Use the route handlers as controller
 ******************************************/ 
app.use('/', viewRoutes);
app.use('/api/v1', apiRoutes);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, () => {
  console.log(`server running on http://${host}:${port} successfully.`);
  logger(`server running on http://${host}:${port} successfully.`);
});

module.exports = app;
